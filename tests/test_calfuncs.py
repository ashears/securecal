# Nose tests for secure calendar.

import nose
import arrow
import csv
import os
from calbackend import *
from caltime import *
from crypt import *


def test_create_and_add():
    crypt.encrypt_and_store("nose_test_calendar", "nose_test_calendar", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    nose_event_map = EventMap("nose_test_calendar", "nose_test_calendar")
    title = "Event one"
    start = "March 30, 2018, 10:13 pm"
    end = "March 30, 2018, 11:13 pm"
    location = "EMU"
    description = "Do an important event."
    notes = "Nothing to see here."
    new_event = CalEvent(title, start, end, location, description, notes)
    nose_event_map.add(new_event)
    assert nose_event_map.events_in_month(2018, 3) == [(30, 1)]
    assert nose_event_map.events_in_day(2018, 3, 30) == [new_event]
    os.remove("nose_test_calendar.cal")
