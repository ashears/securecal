import nose
import arrow
import csv
import os
from calbackend import *
from caltime import *
from crypt import *

today = arrow.now() #getting today's date to access tsv file


def test_add() -> None:
    '''
    input: None
    return Value: None
    Nose test function for test add, sort, read and write tsv.
    Testing add, sort, read and write tsv after add 30 events on same time.

    deleting .cal file using delete_user()

    assert test
    test 1: check it stored all the events that user added
    test 2: check it stored correct amount of events in month with medium cases
    test 3: check it stroed correct amount of events in day with medium cases
    test 4: check it stored correct amount of events in day after the new access with medium cases
    test 5: chekc it stored correct amount of events in month after the new access with medium cases
    '''
    events = []
    #events list for assert test events_in_day() function, it would store Event.
    month = []
    #month list for assert test events_in_month() function, it would store tuple.
    crypt.encrypt_and_store("test_cal", "test_cal", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    nose_event_map = EventMap("test_cal", "test_cal")
    for i in range(1, 31):
        title = "Event {}".format(i)
        start = "{} {}, {}, 4:37 pm".format("April", i, "2018")
        end = "{} {}, {}, 5:00 pm".format("April", i, "2018")
        location = "location"
        description = "description"
        notes = "notes"
        new_event = CalEvent(title, start, end, location, description, notes)
        nose_event_map.add(new_event)
        events.append(new_event)
        month.append((i, 1))

    assert len(nose_event_map.events_in_month(2018, 4)) == 30

    for i in range(1, 31):
        assert nose_event_map.events_in_month(2018, 4) == month
        assert nose_event_map.events_in_day(2018, 4, i) == [events[i-1]]

    #write and read tsv
    nose_event_map.write_tsv()
    nose_event_map.write_encrypted()
    nose_event_map.read_tsv('test_cal-backup-{}-{}-{}.tsv'.format(today.year, today.month, today.day))

    new_nose_event_map = EventMap("test_cal", "test_cal")
    new_nose_event_map.read_tsv("test_cal-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

    #data from tsv
    for i in range(1, 31):
        assert len(new_nose_event_map.events_in_day(2018, 4, i)) == 1
        assert new_nose_event_map.events_in_month(2018, 4) == month

    new_nose_event_map.delete_user() #delete .cal file
    os.remove("test_cal-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))


def test_add_sort_large() -> None:
    '''
    input: None
    return Value: None
    Nose test function for test add, sort, read and write tsv.
    Testing add, sort, read and write tsv after add 38940 events.
    Which means that this function would make event per minute.

    deleting .cal file using delete_user()

    assert test
    test 1: Test the program automatically ignored the duplicate title
    and added all events with good format with large cases
    test 2: Test the program that automatically ignored the duplicate title
    and addded all events after new access with large cases
    test 3: Test the program if it returns correct amount of day that contains event
    test 4: Test the program if it returns correct amount of day that contains event after new access
    '''

    #try to push over 30000 events with duplicate title
    list1 = []  #this is for assert test, to check the dictionary is sorted or not.
    crypt.encrypt_and_store("test_cal_large", "test_cal_large", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    nose_event_map = EventMap("test_cal_large", "test_cal_large")
    for k in range(1, 31):     #day
        am_list = []
        #put morning events in this list
        pm_list = []
        #put afternoon events in this list
        for i in range(1, 12):   #hour
            for j in range(0, 59):   #minute
                title = "Event {}".format(str(j))
                start_am = "April {}, 2018, {}:{:02d} am".format(k, i, j)
                end_am = "April {}, 2018, {}:{:02d} am".format(k, i, j+1)
                start_pm = "April {}, 2018, {}:{:02d} pm".format(k, i, j)
                end_pm = "April {}, 2018, {}:{:02d} pm".format(k, i, j+1)
                location = "location"
                description = "description"
                notes = "notes"
                new_event_am = CalEvent(title, start_am, end_am, location, description, notes)
                new_event_pm = CalEvent(title, start_pm, end_pm, location, description, notes)
                nose_event_map.add(new_event_pm)
                nose_event_map.add(new_event_am)


    #write and read tsv
    nose_event_map.write_tsv()
    nose_event_map.write_encrypted()
    nose_event_map.read_tsv("test_cal_large-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

    #checking the system that have correct events after the new access
    new_access = EventMap("test_cal_large", "test_cal_large")
    new_access.read_tsv("test_cal_large-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

    #print(len(list1[i-1]))
    #checking events
    for i in range(1, 31):
        assert len(nose_event_map.events_in_day(2018, 4, i)) == 59
        assert len(new_access.events_in_day(2018, 4, i)) == 59

    assert len(nose_event_map.events_in_month(2018, 4)) == 30
    assert len(new_access.events_in_month(2018, 4)) == 30


    new_access.delete_user() #delete .cal file
    os.remove("test_cal_large-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

def test_add_sort_small() -> None:
    '''
    input: None
    return Value: None
    Nose test function for test add, sort, read and write tsv.
    Testing add, sort, read and write tsv after add two events on April 22, one event on April 23

    deleting .cal file using delete_user()

    assert test:
    test 1: Check it stored correct amount of events in day with small cases
    test 2: Check it stored correct amount of events in day(different day) with small cases
    test 3: Check it stroed correct amount of events in month with small cases
    test 4: Check it stored correct amount of events in day after new access
    test 5: Check it stored correct amount of events in day after new access
    '''

    crypt.encrypt_and_store("test_cal_small", "test_cal_small", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    nose_event_map = EventMap("test_cal_small", "test_cal_small")
    title1 = "Event 1"
    title2 = "Event 2"
    title3 = "Event 3"
    start_am = "April 22, 2018, 1:00 am"
    end_am = "April 22, 2018, 2:00 am"
    start_pm = "April 22, 2018, 1:00 pm"
    end_pm = "April 22, 2018, 2:00 pm"
    start2 = "April 23, 2018, 1:00 am"
    end2 = "April 23, 2018, 2:00 am"
    location = "location"
    description = "description"
    notes = "notes"
    new_event_am = CalEvent(title1, start_am, end_am, location, description, notes)
    new_event_pm = CalEvent(title2, start_pm, end_pm, location, description, notes)
    new = CalEvent(title3, start2, end2, location, description, notes)
    nose_event_map.add(new_event_pm)
    nose_event_map.add(new_event_am)
    nose_event_map.add(new)


    #write and read tsv
    nose_event_map.write_tsv()
    nose_event_map.write_encrypted()
    nose_event_map.read_tsv("test_cal_small-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

    #new access to calendar
    new_access = EventMap('test_cal_small', 'test_cal_small')
    new_access.read_tsv("test_cal_small-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))


    assert nose_event_map.events_in_day(2018, 4, 22) == [new_event_am, new_event_pm]
    assert nose_event_map.events_in_day(2018, 4, 23) == [new]
    assert nose_event_map.events_in_month(2018, 4) == [(22, 2), (23, 1)]
    assert len(new_access.events_in_day(2018, 4, 22)) == 2
    assert len(new_access.events_in_day(2018, 4, 23)) == 1

    new_access.delete_user() #delete .cal file
    os.remove("test_cal_small-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))


def test_pop() -> None:
    '''
    input: None
    output: None
    Testing the pop() method. Check pop() is working correctly and write correctly in tsv

    deleting .cal file using delete_user()

    assert test
    test 1: Check the pop() functioned correctly.
    test 2: Check the program contains correct remained events after pop().
    test 3: Checking events after new aceess to calendar.
    '''

    crypt.encrypt_and_store("test_cal", "test_cal", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    nose_event_map = EventMap("test_cal", "test_cal")
    title1 = "Event 1"
    title2 = "Event 2"
    title3 = "Event 3"
    start_am = "April 22, 2018, 1:00 am"
    end_am = "April 22, 2018, 2:00 am"
    start_pm = "April 22, 2018, 1:00 pm"
    end_pm = "April 22, 2018, 2:00 pm"
    start2 = "April 23, 2018, 1:00 am"
    end2 = "April 23, 2018, 2:00 am"
    location = "location"
    description = "description"
    notes = "notes"
    new1 = CalEvent(title1, start_am, end_am, location, description, notes)
    new2 = CalEvent(title2, start_pm, end_pm, location, description, notes)
    new3 = CalEvent(title3, start2, end2, location, description, notes)
    nose_event_map.add(new1)
    nose_event_map.add(new2)
    nose_event_map.add(new3)

    #pop one event
    nose_event_map.pop(new1)

    #write tsv to save events
    nose_event_map.write_tsv()
    nose_event_map.write_encrypted()

    #new access to calendar events
    new_access = EventMap("test_cal", "test_cal")
    new_access.read_tsv("test_cal-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))



    #check the program contain correct number of events
    assert nose_event_map.events_in_month(2018, 4)[0][1] == 1
    assert len(nose_event_map.events_in_month(2018, 4)) == 2
    assert len(new_access.events_in_day(2018, 4, 22)) == 1

    #remove all the tested files
    new_access.delete_user() #delete .cal file
    os.remove("test_cal-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

def test_multusers() -> None:
    '''
    input = None
    output = None
    *this function is for nose testing

    test the program could add,pop for correct user and access correct tsv
    for multiple user access.

    deleting .cal file using delete_user()

    assert test:
    test 1: Check user1 have correct amount of events in month
    test 2: Check user1 have correct amount of events in day after pop()
    test 3: Check user2 have correct amount of events in day after pop()
    test 4: Check user3 have correct amount of events in day after pop()
    test 5: Check user2 have correct amount of events on April 21st after pop()
    '''
    crypt.encrypt_and_store("user1", "user1", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    crypt.encrypt_and_store("user2", "user2", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    crypt.encrypt_and_store("user3", "user3", "Title\tStart\tEnd\tLocation\tDescription\tNotes" + os.linesep)
    #creating three users
    eventmap1 = EventMap("user1", "user1")
    eventmap2 = EventMap("user2", "user2")
    eventmap3 = EventMap("user3", "user3")
    #creating three Eventmap

    event_list = []
    for i in range(1, 6):
        title = "Event {}".format(i)
        start = "April 2{}, 2018, {}:00 pm".format(i, i)
        end = "April 2{}, 2018, {}:00 pm".format(i, i+1) #create one hour event
        location = "location"
        description = "description"
        notes = "notes"
        event = CalEvent(title, start, end, location, description, notes)
        eventmap1.add(event)
        eventmap2.add(event)
        eventmap3.add(event)
        event_list.append(event)

    #write tsv backup file of every users
    eventmap1.write_tsv()
    eventmap2.write_tsv()
    eventmap3.write_tsv()

    eventmap1.write_encrypted()
    eventmap2.write_encrypted()
    eventmap3.write_encrypted()


    #accessing with id and password again
    new_event1 = EventMap("user1", "user1")
    new_event2 = EventMap("user2", "user2")
    new_event3 = EventMap("user3", "user3")
    new_event1.read_tsv("user1-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))
    new_event2.read_tsv("user2-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))
    new_event3.read_tsv("user3-backup-{}-{}-{}.tsv".format(today.year, today.month, today.day))

    #check all the events correctly access data
    assert len(new_event1.events_in_month(2018, 4)) == 5

    new_event1.pop(event_list[0])
    new_event2.pop(event_list[1])

    assert new_event1.events_in_day(2018, 4, 21) is None
    assert new_event2.events_in_day(2018, 4, 22) is None
    assert len(new_event3.events_in_day(2018, 4, 21)) == 1
    assert len(new_event2.events_in_day(2018, 4, 21)) == 1

    #removeing all tested files
    new_event1.delete_user()
    new_event2.delete_user()
    new_event3.delete_user()
    for i in range(1, 4):
        os.remove("user{}-backup-{}-{}-{}.tsv".format(i, today.year, today.month, today.day))
        #remove all .tsv file
